package com.lms.ms.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import feign.RequestTemplate;

@SpringBootApplication
@EnableFeignClients
public class UserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}
	/*
	 * @Bean AuthInterceptor authFeign() { return new AuthInterceptor(); }
	 * 
	 * class AuthInterceptor implements RequestInterceptor {
	 * 
	 * @Override public void apply(RequestTemplate template) {
	 * template.header("Authorization", "<your token>");
	 * 
	 * }
	 * 
	 * }
	 */
}
