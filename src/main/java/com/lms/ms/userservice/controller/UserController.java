package com.lms.ms.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lms.ms.userservice.entity.User;
import com.lms.ms.userservice.model.UserRequestModel;
import com.lms.ms.userservice.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("/{name}")
	public ResponseEntity<User> getUser(@PathVariable("name") String name) {
		return ResponseEntity.ok(userService.get(name));
	}

	@PostMapping("/")
	public ResponseEntity<?> createUser(@RequestBody UserRequestModel userRequest) {
		userService.save(userRequest);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<User> updateUser(@RequestBody UserRequestModel userRequest) {
		return ResponseEntity.ok(userService.update(userRequest));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable("id") String id) {
		userService.deleteUser(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

}
