package com.lms.ms.userservice.exception;

public class BadRequestDataException extends RuntimeException {

	private static final long serialVersionUID = 5570040781371521268L;

	public BadRequestDataException(String msg) {

		super(msg);
	}
}
