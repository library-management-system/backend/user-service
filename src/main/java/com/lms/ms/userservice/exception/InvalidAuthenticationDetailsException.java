package com.lms.ms.userservice.exception;

public class InvalidAuthenticationDetailsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidAuthenticationDetailsException() {
		super("Invalid Token");
	}

	public InvalidAuthenticationDetailsException(String message) {
		super(message);
	}
}
