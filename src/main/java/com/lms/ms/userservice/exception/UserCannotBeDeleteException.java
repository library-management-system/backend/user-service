package com.lms.ms.userservice.exception;

public class UserCannotBeDeleteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserCannotBeDeleteException(String msg) {
		super(msg);
	}
}
