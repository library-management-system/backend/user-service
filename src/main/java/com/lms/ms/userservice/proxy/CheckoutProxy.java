package com.lms.ms.userservice.proxy;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "checkout-service", url = "http://localhost:8082/")
public interface CheckoutProxy {

	@GetMapping("/checkout/books/status/active/{userName}")
	public Map<String, Object> getActiveBooksOfuser(@PathVariable("userName") String userName,
			@RequestHeader("Authorization") String token);
}
