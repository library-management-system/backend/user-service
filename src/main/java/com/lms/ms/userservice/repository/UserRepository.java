package com.lms.ms.userservice.repository;

import java.util.Map;
import java.util.Optional;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.ms.userservice.MongoConfig;
import com.lms.ms.userservice.entity.User;
import com.lms.ms.userservice.exception.UserNotFoundException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;
import com.mongodb.client.result.InsertOneResult;

@Component
public class UserRepository {

	@Autowired
	MongoConfig mongoConfig;

	ObjectMapper objectMapper = new ObjectMapper();

	public Optional<User> findByUserName(String name) {
		MongoCollection<Document> collection = mongoConfig.getUserCollection();
		FindIterable<Document> result = collection.find(Filters.eq("_id", name));
		Document user = result.first();
		if (user == null) {
			return Optional.empty();
		}
		user.remove("_id");
		User userDetails = objectMapper.convertValue(user, User.class);
		return Optional.of(userDetails);
	}

	@SuppressWarnings("unchecked")
	public void save(User user) {
		MongoCollection<Document> collection = mongoConfig.getUserCollection();
		Map<String, Object> userMap = objectMapper.convertValue(user, Map.class);
		userMap.put("_id", user.getUserName());
		InsertOneResult result = collection.insertOne(new Document(userMap));
		if (result.wasAcknowledged()) {

		}

	}

	public User update(User user) {
		MongoCollection<Document> collection = mongoConfig.getUserCollection();
		Map<String, Object> userMap = objectMapper.convertValue(user, Map.class);
		userMap.put("_id", user.getUserName());
		FindOneAndReplaceOptions options = new FindOneAndReplaceOptions();
		options.returnDocument(ReturnDocument.AFTER);
		Document updatedUser = collection.findOneAndReplace(Filters.eq("_id", user.getUserName()),
				new Document(userMap), options);
		updatedUser.remove("_id");
		return objectMapper.convertValue(updatedUser, User.class);
	}

	public Optional<User> delete(String userName) {
		MongoCollection<Document> collection = mongoConfig.getUserCollection();
		Document deletedUser = collection.findOneAndDelete(Filters.eq("_id", userName));
		if (deletedUser == null) {
			return Optional.empty();
		}
		deletedUser.remove("_id");
		User userDetails = objectMapper.convertValue(deletedUser, User.class);
		return Optional.of(userDetails);

	}
}
