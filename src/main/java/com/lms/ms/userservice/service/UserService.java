package com.lms.ms.userservice.service;

import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pmw.tinylog.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lms.ms.userservice.RequestContext;
import com.lms.ms.userservice.entity.User;
import com.lms.ms.userservice.exception.BadRequestDataException;
import com.lms.ms.userservice.exception.UserCannotBeDeleteException;
import com.lms.ms.userservice.exception.UserNotFoundException;
import com.lms.ms.userservice.model.UserRequestModel;
import com.lms.ms.userservice.proxy.CheckoutProxy;
import com.lms.ms.userservice.repository.UserRepository;
import com.lms.ms.userservice.utils.UserUtils;
import com.lms.ms.userservice.utils.ValidationUtils;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	CheckoutProxy checkoutClient;
	@Autowired
	RequestContext context;

	public User get(String name) {
		Optional<User> userDetails = userRepository.findByUserName(name);
		userDetails.orElseThrow(() -> new UserNotFoundException("User not found with name:" + name));
		return userDetails.get();
	}

	public void save(UserRequestModel userRequest) {
		ValidationUtils.validatePhoneNumber.test(userRequest.getPhoneNumber());
		ValidationUtils.validateEmail.test(userRequest.getGmail());
		User user = UserUtils.convertUserRequestModelToUserEntity.apply(userRequest);
		userRepository.save(user);
	}

	public User update(UserRequestModel userRequest) {
		ValidationUtils.validatePhoneNumber.test(userRequest.getPhoneNumber());
		ValidationUtils.validateEmail.test(userRequest.getGmail());
		User user = get(userRequest.getUserName());
		User updatedUser = UserUtils.converUserRequestToUpdate.apply(userRequest, user);
		return userRepository.update(updatedUser);
	}

	public void deleteUser(String userName) {
		Map<String, Object> activeBooksOfuser = checkoutClient.getActiveBooksOfuser(userName, context.getToken());
		if (!activeBooksOfuser.isEmpty()) {
			throw new UserCannotBeDeleteException("User have active books.Please return book before delete user");
		}
		Optional<User> deletedUser = userRepository.delete(userName);
		deletedUser.orElseThrow(() -> new UserNotFoundException("User not found with name:" + userName));
		Logger.info("User deleted successfully");

	}

}
