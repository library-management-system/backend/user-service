package com.lms.ms.userservice.utils;

import java.util.function.BiFunction;
import java.util.function.Function;

import com.lms.ms.userservice.entity.User;
import com.lms.ms.userservice.model.UserRequestModel;

public class UserUtils {

	private static Function<String, String> hashPassword = (plainPassword) -> {
		return plainPassword;
	};

	public static Function<UserRequestModel, User> convertUserRequestModelToUserEntity = userRequestModel -> {
		User user = new User();
		user.setUserName(userRequestModel.getUserName());
		user.setGmail(userRequestModel.getGmail());
		user.setPassword(hashPassword.apply(userRequestModel.getPassword()));
		user.setPhoneNumber(userRequestModel.getPhoneNumber());
		return user;
	};

	public static BiFunction<UserRequestModel, User, User> converUserRequestToUpdate = (userUpdateModel, savedUser) -> {

		User user = new User();
		user.setUserName(savedUser.getUserName());
		user.setGmail(savedUser.getGmail());
		user.setPassword(hashPassword.apply(userUpdateModel.getPassword()));
		user.setPhoneNumber(userUpdateModel.getPhoneNumber());
		return user;
	};

}
