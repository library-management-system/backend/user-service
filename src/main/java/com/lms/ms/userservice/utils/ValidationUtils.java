package com.lms.ms.userservice.utils;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lms.ms.userservice.exception.BadRequestDataException;

public class ValidationUtils {

	public static String EMAIL_PATTERN = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$";
	public static String PHONE_NUMBER_PATTERN = "^[789][0-9]{9}$";

	public static Pattern phoneNumberPattern = Pattern.compile(PHONE_NUMBER_PATTERN);
	public static Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);

	public static Predicate<Integer> validatePhoneNumber = (phoneNumber) -> {
		Matcher matcher = phoneNumberPattern.matcher(Integer.toString(phoneNumber));
		if (!matcher.find()) {
			throw new BadRequestDataException("Please provide valid phoneNumber");
		}
		return true;
	};

	public static Predicate<String> validateEmail = (email) -> {
		Matcher matcher = emailPattern.matcher(email);
		if (!matcher.find()) {
			throw new BadRequestDataException("Please provide valid email address");
		}
		return true;
	};
}
